package org.academiadecodigo.tailormoons.histrogram_word;

import java.util.HashMap;
import java.util.Iterator;

public class ByComposition implements Iterable<String> {

    private HashMap<String, Integer> hashMap = new HashMap<>();


    public void add(String text) {

        String[] strings = text.split(" ");

        for (String string : strings) {

            if (hashMap.containsKey(string)) {
                hashMap.put(string, hashMap.get(string) + 1);
                continue;
            }
            hashMap.put(string, 1);
        }

    }


    public int countWord(String text) {
        return hashMap.get(text);
    }


    @Override
    public Iterator<String> iterator() {
        return hashMap.keySet().iterator();
    }
}
