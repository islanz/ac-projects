package org.academiadecodigo.tailormoons.histrogram_word;

import java.util.HashMap;
import java.util.Iterator;

public class ByInheritance extends HashMap<String, Integer> implements Iterable<String> {


    public void add(String text) {

        String[] strings = text.split(" ");

        for (String string : strings) {

            if (containsKey(string)) {
                put(string, get(string) + 1);
                continue;
            }
            put(string, 1);
        }

    }


    public int countWord(String text) {
        return get(text);
    }


    @Override
    public Iterator<String> iterator() {
        return keySet().iterator();
    }
}
