package org.academiadecodigo.tailormoons.chat_multithread.client.connection;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Sender implements Runnable {

    private final PrintWriter out;
    private final Socket clientSocket;


    public Sender(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        out = new PrintWriter(clientSocket.getOutputStream(), true);
    }


    @Override
    public void run() {

        Scanner scanner = new Scanner(System.in);

        while (!clientSocket.isClosed()) {
            String message = scanner.nextLine();
            send(message);

            if (message.startsWith("/quit")) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    private void send(String message) {
        out.println(message);
    }

}
