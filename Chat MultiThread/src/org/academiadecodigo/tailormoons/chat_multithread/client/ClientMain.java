package org.academiadecodigo.tailormoons.chat_multithread.client;

import java.io.IOException;

public class ClientMain {

    public static void main(String[] args) {

        String host = "127.0.0.1";
        int port = 8181;

        if (args.length > 0) {
            host = args[0];
            if (args.length > 1) {
                try {
                    port = Integer.parseInt(args[1]);
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid port number. " + ex.toString());
                }
            }
        }


        Client client;
        try {
            client = new Client(host, port);
            client.start();
        } catch (IOException ex) {
            System.out.println("Error opening client. " + ex.toString());
        }

    }

}
