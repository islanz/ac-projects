package org.academiadecodigo.tailormoons.chat_multithread.client;

import org.academiadecodigo.tailormoons.chat_multithread.client.connection.Receiver;
import org.academiadecodigo.tailormoons.chat_multithread.client.connection.Sender;

import java.io.IOException;
import java.net.Socket;

public class Client {

    private final Socket clientSocket;


    public Client(String host, int port) throws IOException {
        clientSocket = new Socket(host, port);
    }


    public void start() {

        try {
            Thread sender = new Thread(new Sender(clientSocket));
            Thread receiver = new Thread(new Receiver(clientSocket));

            sender.start();
            receiver.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
