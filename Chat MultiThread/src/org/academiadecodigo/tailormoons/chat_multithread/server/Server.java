package org.academiadecodigo.tailormoons.chat_multithread.server;

import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.ClientManager;
import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.User;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private final ServerSocket serverSocket;
    private final ClientManager clientManager = new ClientManager();
    private int port;

    public Server(int port) throws IOException {
        this.port = port;
        serverSocket = new ServerSocket(port);
    }


    public void start() {

        Socket clientSocket;
        User user;
        ExecutorService service = Executors.newFixedThreadPool(50);

        while (!serverSocket.isClosed()) {

            try {
                clientSocket = serverSocket.accept();
                System.out.println("New client accepted");
            } catch (IOException exception) {
                System.out.println("Error getting new user");
                continue;
            }

            user = new User(clientSocket, clientManager);
            service.execute(user);

        }

        service.shutdownNow();

    }

}
