package org.academiadecodigo.tailormoons.chat_multithread.server.client_connection;

import org.academiadecodigo.tailormoons.chat_multithread.server.command.Command;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ClientManager {

    private final List<User> users = Collections.synchronizedList(new LinkedList<>());
    private int counter = 0;


    public void add(User user) {
        users.add(user);
    }


    public void receive(String message, User user) {
        if (message.startsWith("/")) {
            command(message, user);
            return;
        }

        broadcast(message, user);
    }


    private void broadcast(String message, User user) {

        synchronized (users) {

            for (User client : users) {

                if (client == user) {
                    continue;
                }

                sendMessageUser(user.getName() + ": " + message, client);
            }
        }
    }


    private void command(String command, User user) {
        command = command.substring(1);
        String[] args = command.split(" ");

        args[0] = args[0].toUpperCase();

        Command commandHandler;
        try {
            commandHandler = Command.valueOf(args[0]);
        } catch (IllegalArgumentException ex) {
            commandHandler = Command.INVALID;
            commandHandler.getCommandHandler().run(user, this, "Invalid command. Use /help");
            return;
        }
        commandHandler.getCommandHandler().run(user, this, (args.length > 1) ? args[1] : "");
    }


    public void sendMessageUser(String message, User user) {
        PrintWriter out;
        try {
            out = new PrintWriter(user.getClientSocket().getOutputStream(), true);
            out.println(message);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }


    public void removeUser(User user) {
        users.remove(user);
    }


    public int getCounter() {
        return counter++;
    }


    public List<User> getUsers() {
        return users;
    }

}
