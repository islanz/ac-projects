package org.academiadecodigo.tailormoons.chat_multithread.server.command;

import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.ClientManager;
import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.User;

public class CommandClose implements CommandHandler {
    @Override
    public void run(User user, ClientManager clientManager, String text) {
        if (user.isAdmin()) {
            //Server.close;
        }
    }
}
