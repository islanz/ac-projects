package org.academiadecodigo.tailormoons.chat_multithread.server.command;

public enum Command {
    HELP(new CommandHelp()),
    NAME(new CommandName()),
    LIST(new CommandList()),
    ADMIN(new CommandAdmin()),
    QUIT(new CommandQuit()),
    CLOSE(new CommandClose()),
    INVALID(new CommandInvalid());

    private final CommandHandler commandHandler;

    Command(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }


    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

}
