package org.academiadecodigo.bootcamp.rockpaperscissor;

public enum HandType {
    ROCK("rock", "paper", "lizard"),
    PAPER("paper", "rock", "spock"),
    SCISSORS("scissors", "paper", "lizard"),
    LIZARD("lizard","paper","spock"),
    SPOCK("spock","rock","scissors");

    private String name;
    private String winAgainst1;
    private String winAgainst2;

    HandType(String name, String winAgainst1, String winAgainst2) {
        this.name = name;
        this.winAgainst1 = winAgainst1;
        this.winAgainst2 = winAgainst2;
    }

    public String getName() {
        return name;
    }

    public String getWinAgainst1() {
        return winAgainst1;
    }

    public String getWinAgainst2() {
        return winAgainst2;
    }

}
