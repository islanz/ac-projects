package org.academiadecodigo.bootcamp.rockpaperscissor;

public class Player {

    private String name;
    private int roundWins;
    private int roundLosses;

    private int wins;
    private int losses;

    public Player(String name) {
        this.name = name;
        roundWins = 0;
        roundLosses = 0;
        wins = 0;
        losses = 0;

    }

    public HandType showHand() {
        HandType choice = Generator.generate();
        return choice;
    }

    public String getName() {
        return name;
    }

    public int getRoundWins() {
        return roundWins;
    }

    public int getRoundLosses() {
        return roundLosses;
    }

    public void setRoundWin() {
        roundWins++;
    }

    public void setRoundLoss() {
        roundLosses++;
    }

    public void resetRounds() {
        roundWins = 0;
        roundLosses = 0;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setWins() {
        wins++;
    }

    public void setLosses() {
        losses++;
    }

}
