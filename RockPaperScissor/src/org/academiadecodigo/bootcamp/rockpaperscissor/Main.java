package org.academiadecodigo.bootcamp.rockpaperscissor;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Player[] players = new Player[2];
        boolean playing = true;

        for (int i = 0; i < 2; i++) {
            System.out.print("Name player " + i + ": ");
            String name = scanner.nextLine();
            players[i] = new Player(name);
        }

        System.out.println("Initializing game " + Game.getGamesCount());

        while (playing) {

            System.out.print("Number of rounds per game: ");
            int roundsNumber = scanner.nextInt();

            Game game = new Game(roundsNumber);

            System.out.println();
            game.startGame(players);

            System.out.println();
            System.out.print("Continue playing? (y/n): ");
            String answer = scanner.next();
            if (answer.equals("n")) {
                playing = false;
            }
            System.out.println();
        }

        System.out.println("Finishing game...");
        System.out.println("Printing results:");
        System.out.println();
        System.out.println(players[0].getName() + " : (" + players[0].getWins() + "-" + players[0].getLosses() + (") (W-L)"));
        System.out.println(players[1].getName() + " : (" + players[1].getWins() + "-" + players[1].getLosses() + (") (W-L)"));

    }
}
