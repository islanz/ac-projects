package org.academiadecodigo.tailormoons.scum;

public interface MonoOperation<T> {

    void operation(T obj);

}
