package org.academiadecodigo.tailormoons.tcpChat.client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private final Socket clientSocket;

    private Scanner scanner;

    public Client(String hostName, int portNumber) throws IOException {
        clientSocket = new Socket(hostName, portNumber);
    }


    public void start() {

        scanner = new Scanner(System.in);
        PrintWriter out;
        BufferedReader in;

        while (!clientSocket.isClosed()) {

            String message = getMessage();
            if (message.equals("/quit")) {
                closeSocket(clientSocket);
                return;
            }

            try {
                //SEND MESSAGE TO SERVER
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                out.println(message);

                //RECEIVE MESSAGE FROM SERVER
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                printMessage(in.readLine());

            } catch (IOException ex) {
                System.out.println("Error creating streams: " + ex.toString());
                return;
            }

        }
    }

    private String getMessage() {
        return scanner.nextLine();
    }

    private void printMessage(String message) {
        System.out.println(message);
    }

    private void closeSocket(Socket socket) {
        try {
            socket.close();
        } catch (IOException ex) {
            System.out.println("Couldn't close the socket properly. " + ex.toString());
        }
    }

}
