package org.academiadeocodigo.tailormoons.unique_word;

public class Main {

    public static void main(String[] args) {

        UniqueWord uniqueWord = new UniqueWord();

        uniqueWord.addString("Eu sou sou o Diogo Diogo");

        for (String word : uniqueWord) {
            System.out.print(word + " ");
        }

    }

}
