package org.academiadeocodigo.tailormoons.unique_word;


import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class UniqueWord implements Iterable<String> {

    private Set<String> hashSet = new LinkedHashSet<>();


    public void addString(String string) {
        String[] words = string.split(" ");

        for(String word : words) {
            hashSet.add(word);
        }
    }



    @Override
    public Iterator<String> iterator() {
        return hashSet.iterator();
    }

}
