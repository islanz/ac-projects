package org.academiadecodigo.tailormoon.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFile {

    public void readFile() throws IOException {

        FileInputStream inputStream = new FileInputStream("textToBeCopied");

        // read one byte from the file
        int b = inputStream.read();

        // test for end of file
        if (b == -1) {
            // no more reading from this file...
        }

        // read multiple bytes from the file
        byte[] buffer = new byte[1024];
        int num = inputStream.read(buffer);

        // print the number of bytes read
        if (num != -1) {
            System.out.println("I have read this many bytes: " + num);
        }

        inputStream.close();

    }
}
