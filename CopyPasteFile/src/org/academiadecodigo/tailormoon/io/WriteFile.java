package org.academiadecodigo.tailormoon.io;

import java.io.FileOutputStream;
import java.io.IOException;

public class WriteFile {

    public void write() throws IOException {

        // open an output stream with a file path as the destination
        FileOutputStream outputStream = new FileOutputStream("file_path");

        // write a single byte to file
        byte b = 0x37;
        outputStream.write(b);

        // write multiple bytes to file
        byte[] buffer = "Some string".getBytes();
        outputStream.write(buffer);

        // don't forget to close the output stream
        outputStream.close();

    }
}
