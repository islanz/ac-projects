import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class FindAddress {

    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println("invalid parameters");
            return;
        }


        String url = "www.google.com";
        int timeout = 500;

        try {
            url = args[0];
            timeout = Integer.parseInt(args[1]);
        } catch (IllegalArgumentException ex) {
            System.out.println("Invalid arguments");
        }


        try {
            InetAddress inetAddress = InetAddress.getByName(url);

            System.out.println("Address: " + inetAddress.getHostAddress());

            if (inetAddress.isReachable(timeout)) {
                System.out.println("This host is reachable");
                return;
            }
            System.out.println("Not reachable");

        } catch (UnknownHostException ex) {
            System.out.println("No host found");

        } catch (IOException e) {   
            e.printStackTrace();

        }

    }

}
