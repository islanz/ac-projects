package org.academiadecodigo.tailormoons.rangeIterator;

import java.util.Iterator;

public class RangeLinkedList implements Iterable<Integer> {

    private LinkedList<Integer> linkedList = new LinkedList<>();

    public RangeLinkedList(int min, int max) {
        for (int i = min; i <= max; i++) {
            linkedList.add(i);
        }
    }


    @Override
    public Iterator<Integer> iterator() {
        return new RangeIteratorLinkedList(linkedList);
    }

}
