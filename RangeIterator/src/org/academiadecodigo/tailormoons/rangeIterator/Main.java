package org.academiadecodigo.tailormoons.rangeIterator;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
/*
        LinkedList<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i <= 5; i++) {
            linkedList.add(i);
        }

        Iterator<Integer> it = linkedList.iterator();
        while (it.hasNext()) {
            System.out.println("iterator 1 is at : " + it.next());

            Iterator<Integer> it2 = linkedList.iterator();
            while (it2.hasNext()) {
                System.out.println("iterator 2 is at : " + it2.next());
            }
        }
*/

        Range range = new Range(20, 30);

        int counter = 0;
        for (int value : range) {
            if(counter > 5) {
                range.setIncrementing();
            }
            System.out.println(value);
            counter++;
        }

        System.out.println();

        for (int value : range) {
            System.out.println(value);
        }
    }

}
