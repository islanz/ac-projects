package org.academiadecodigo.tailormoons.bit_mapping.keyboard;


public interface Interactive {

    void keyPressed(int key);


    void setPressed(boolean pressed);


    void setMoving(int key, boolean moving);

}
