package org.academiadecodigo.tailormoons.bit_mapping;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.tailormoons.bit_mapping.cursor.Cursor;
import org.academiadecodigo.tailormoons.bit_mapping.grid.ColorEnum;
import org.academiadecodigo.tailormoons.bit_mapping.grid.Direction;
import org.academiadecodigo.tailormoons.bit_mapping.grid.Grid;
import org.academiadecodigo.tailormoons.bit_mapping.keyboard.Interactive;
import org.academiadecodigo.tailormoons.bit_mapping.keyboard.KeyboardListener;
import org.academiadecodigo.tailormoons.bit_mapping.save.SaveHandler;

import java.io.IOException;

public class Manager implements Interactive {

    public static int WIDTH = 20;
    public static int HEIGHT = 20;

    private Grid grid;

    private Cursor cursor;

    private SaveHandler saveHandler;

    private ColorEnum color = ColorEnum.BLACK;

    private boolean spacePressed;

    private boolean movingUp;

    private boolean movingDown;

    private boolean movingLeft;

    private boolean movingRight;

    public Manager() {
    }


    public void init() {

        grid = new Grid(WIDTH, HEIGHT);
        grid.init();

        cursor = new Cursor();

        saveHandler = new SaveHandler();

        KeyboardListener keyboardListener = new KeyboardListener(this);
    }


    public void keyPressed(int key) {

        if(movingUp) {
            cursor.move(Direction.UP);
        }

        if(movingDown) {
            cursor.move(Direction.DOWN);
        }

        if(movingLeft) {
            cursor.move(Direction.LEFT);
        }

        if(movingRight) {
            cursor.move(Direction.RIGHT);
        }

        if (spacePressed) {
            grid.drawCell(cursor.getPosition().getX(), cursor.getPosition().getY(), ColorEnum.BLACK);
        }

        switch (key) {
            case KeyboardEvent.KEY_C:
                grid.clear();
                break;

            case KeyboardEvent.KEY_S:
                saveHandler.save(grid.toString());
                break;

            case KeyboardEvent.KEY_L:
                grid.loadGrid(saveHandler.loadSave());
                break;

            case KeyboardEvent.KEY_D:
                saveHandler.deleteFile();
                break;

            case KeyboardEvent.KEY_Q:
                try {
                    System.in.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
        }

    }


    @Override
    public void setPressed(boolean pressed) {
        spacePressed = pressed;
    }


    @Override
    public void setMoving(int key, boolean moving) {
        switch (key) {
            case KeyboardEvent.KEY_UP:
                movingUp = moving;
                break;
            case KeyboardEvent.KEY_DOWN:
                movingDown = moving;
                break;
            case KeyboardEvent.KEY_LEFT:
                movingLeft = moving;
                break;
            case KeyboardEvent.KEY_RIGHT:
                movingRight = moving;
                break;
        }

    }

}
