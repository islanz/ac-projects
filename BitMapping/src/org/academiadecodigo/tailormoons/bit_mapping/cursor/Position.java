package org.academiadecodigo.tailormoons.bit_mapping.cursor;

import org.academiadecodigo.tailormoons.bit_mapping.Manager;

public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public void setCoordinates(int moveX, int moveY) {
        if (x + moveX < 0 || x + moveX >= Manager.WIDTH || y + moveY < 0 || y + moveY >= Manager.HEIGHT) {
            return;
        }
        x += moveX;
        y += moveY;
    }


    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }

}
