package org.academiadecodigo.tailormoons.bit_mapping.cursor;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.tailormoons.bit_mapping.grid.Direction;
import org.academiadecodigo.tailormoons.bit_mapping.grid.Grid;

public class Cursor {

    private final Rectangle rectangle;

    private final Position position = new Position(0, 0);

    public Cursor() {
        rectangle = new Rectangle(Grid.PADDING, Grid.PADDING, Grid.CELL_SIZE, Grid.CELL_SIZE);
        rectangle.fill();
    }


    public void move(Direction direction) {
        int initialX = position.getX();
        int initialY = position.getY();

        position.setCoordinates(direction.getMoveX(), direction.getMoveY());

        rectangle.translate((position.getX() - initialX) * Grid.CELL_SIZE, (position.getY() - initialY) * Grid.CELL_SIZE);
    }


    public Position getPosition() {
        return position;
    }
}
