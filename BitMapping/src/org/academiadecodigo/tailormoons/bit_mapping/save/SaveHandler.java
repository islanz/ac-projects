package org.academiadecodigo.tailormoons.bit_mapping.save;

import java.io.*;

public class SaveHandler {


    public void save(String text) {

        FileWriter fileWriter;
        BufferedWriter bufferedWriter = null;

        try {
            fileWriter = new FileWriter("imageSave");

            bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(text);

        } catch (FileNotFoundException ex) {
            System.out.println("File not found");

        } catch (IOException e) {
            e.printStackTrace();

        }

        close(bufferedWriter);

    }


    public String loadSave() {

        BufferedReader bufferedReader = null;
        String load = "";

        try {
            bufferedReader = new BufferedReader(new FileReader("imageSave"));

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                load += line + "\n";
            }

        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");

        } catch (IOException ex) {
            System.out.println(ex.toString());
        }

        close(bufferedReader);

        return load;
    }


    public void deleteFile() {

        BufferedWriter bufferedWriter = null;

        try {
            bufferedWriter = new BufferedWriter(new FileWriter("imageSave"));

            bufferedWriter.write("");

        } catch (IOException ex) {
            System.out.println(ex.toString());
        }

        close(bufferedWriter);
    }


    public void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }

        } catch (IOException ex) {
            System.out.println();
        }
    }

}
