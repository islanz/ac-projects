package org.academiadecodigo.bootcamp.magiclamp;

public class MagicLamp {

    private final int numberGenies;
    private int geniesSummoned = 0;
    private int demonsSummoned = 0;


    public MagicLamp(int numberGenies) {
        this.numberGenies = numberGenies;
    }


    public Genie rub() {

        setGeniesSummoned();
        if (numberGenies < geniesSummoned) {
            //Summon demon
            setDemonsSummoned();
            resetGeniesSummoned();
            RecyclableDemon recyclableDemon = new RecyclableDemon(0, "Demon " + demonsSummoned);
            return recyclableDemon;
        }

        if (geniesSummoned % 2 == 0) {
            //genie odd
            GrumpyGenie grumpyGenie = new GrumpyGenie(3, "genie " + geniesSummoned);
            return grumpyGenie;
        }

        //genie even
        FriendlyGenie friendlyGenie = new FriendlyGenie();
        return friendlyGenie;
    }


    private void setGeniesSummoned() {
        geniesSummoned++;
    }


    public void resetGeniesSummoned() {
        geniesSummoned = 0;
    }


    private void setDemonsSummoned() {
        demonsSummoned++;
    }


    public int getNumberGenies() {
        return numberGenies;
    }


    public int getGeniesSummoned() {
        return geniesSummoned;
    }


    public int getDemonsSummoned() {
        return demonsSummoned;
    }

}
