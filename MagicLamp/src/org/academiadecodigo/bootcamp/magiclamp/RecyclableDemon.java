package org.academiadecodigo.bootcamp.magiclamp;

public class RecyclableDemon extends Genie {


    public RecyclableDemon(int wishesMax, String name) {
        super(wishesMax, name);
    }

    @Override
    public boolean hasWishes(MagicLamp magicLamp) {
        if (super.getWishesGranted() == 5) {
            System.out.println("Choose to recycle");
            magicLamp.resetGeniesSummoned();
            return false;
        }
        return true;
    }

}
