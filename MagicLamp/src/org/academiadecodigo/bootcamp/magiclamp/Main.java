package org.academiadecodigo.bootcamp.magiclamp;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int maxLamps = 5;
        MagicLamp[] magicLamp = new MagicLamp[maxLamps];

        for (int i = 0; i < maxLamps; i++) {
            System.out.println("How many genies on lamp " + (i + 1) + "?");
            int numberGenies = scanner.nextInt();
            magicLamp[i] = new MagicLamp(numberGenies);
        }

        while (true) {

            System.out.println("[numbers] - choose the lamp to rub; \"0\" - menu compare");
            int option = scanner.nextInt();

            if (option == 0) {
                System.out.println("lamp: ");
                int lamp1 = scanner.nextInt();
                System.out.println("lamp: ");
                int lamp2 = scanner.nextInt();

                //COMPARE LAMPS
                if (magicLamp[lamp1].getNumberGenies() == magicLamp[lamp2].getNumberGenies()) {
                    if (magicLamp[lamp1].getGeniesSummoned() == magicLamp[lamp2].getGeniesSummoned()) {
                        if (magicLamp[lamp1].getDemonsSummoned() == magicLamp[lamp2].getDemonsSummoned()) {
                            System.out.println("lamps are the same");
                            continue;
                        }
                    }
                }
                System.out.println("lamps are not the same!");
            }


            Genie genie = magicLamp[option - 1].rub();

            while (genie.grantWish(magicLamp[option-1])) {
                System.out.println("wish granted");
                scanner.next();
            }

        }
    }
}
