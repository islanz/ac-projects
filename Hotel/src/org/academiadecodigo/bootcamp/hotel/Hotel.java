package org.academiadecodigo.bootcamp.hotel;

public class Hotel {

    private Room[] rooms = new Room[10];

    public Hotel() {

        for (int i = 0; i < 10; i++) {
            rooms[i] = new Room(i + 100, true);
        }
    }

    public int checkIn() {

        int roomNumber = getRoom();
        if (roomNumber == -1) {
            print("No rooms available");
            return -1;
        }

        print("Your room number is " + rooms[roomNumber].getNumber());
        rooms[roomNumber].setFree(false);
        return rooms[roomNumber].getNumber();
    }

    public void checkOut(int room) {

        if (room == -1) {
            print("get a room first");
            return;
        }

        print("We hope you enjoyed the time you spent in our hotel. Have a good day");
        rooms[room - 100].setFree(true);
    }

    private int getRoom() {

        for (int i = 0; i < 10; i++) {

            if (rooms[i].getFree()) {
                return i;
            }
        }

        return -1;
    }

    private void print(String msg) {
        System.out.println(msg);
    }

}
