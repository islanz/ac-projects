package org.academiadecodigo.bootcamp.hotel;

public class Room {

    private int number;
    private boolean free;

    public Room(int number, boolean free) {
        this.number = number;
        this.free = free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public boolean getFree() {
        return free;
    }

    public int getNumber() {
        return number;
    }

}
