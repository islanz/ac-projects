package org.academiadecodigo.bootcamp.hotel;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Hotel hotel = new Hotel();
        Person person1 = new Person(hotel);

        Scanner scanner = new Scanner(System.in);

        person1.goHotel();
        scanner.nextLine();
        person1.leaveHotel();

    }

}
