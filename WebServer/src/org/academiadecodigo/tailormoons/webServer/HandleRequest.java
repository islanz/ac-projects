package org.academiadecodigo.tailormoons.webServer;

import java.io.*;
import java.net.Socket;

public class HandleRequest implements Runnable {

    private final Socket clientSocket;

    public HandleRequest(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        String request = getRequest();
        response(request);
    }

    private String getRequest() {

        String request = "";
        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            request = in.readLine();
        } catch (IOException ex) {
            System.out.println("Error reading stream. " + ex.toString());
        }

        return request;
    }


    private void response(String request) {

        OutputStream out;

        try {
            out = clientSocket.getOutputStream();
        } catch (IOException ex) {
            System.out.println("Error opening output stream. " + ex.toString());
            return;
        }


        if (request.startsWith("GET")) {

            ResponseBuilder responseBuilder;
            try {
                responseBuilder = new ResponseBuilder(request);
            } catch (IllegalArgumentException ex) {
                System.out.println("Wrong arguments in header request");
                return;
            }

            try {
                out.write(responseBuilder.getHeader());
                responseBuilder.getContent(out);

            } catch (IOException ex) {
                System.out.println("Error sending response. " + ex.toString());

            } finally {
                close(clientSocket);
            }

        }

    }


    private void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException ex) {
            System.out.println("Can't close the stream" + ex.toString());
        }
    }

}
