package org.academiadecodigo.tailormoons.webServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Listener {

    private final ServerSocket serverSocket;

    public Listener(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }


    public void start() {

        ExecutorService executorService = Executors.newFixedThreadPool(1000);

        while (!serverSocket.isClosed()) {

            try {
                Socket clientSocket = serverSocket.accept();

                executorService.submit(new HandleRequest(clientSocket));
                //System.out.println(Arrays.toString(clientSocket.getInetAddress().getAddress()));

            } catch (IOException ex) {
                System.out.println("Error accepting new connections. " + ex.toString());
            }

        }

        executorService.shutdownNow();

    }

}