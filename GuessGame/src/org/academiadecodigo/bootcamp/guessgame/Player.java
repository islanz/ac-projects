package org.academiadecodigo.bootcamp.guessgame;

public class Player {

    private String name;
    private int[] guessed = new int[Game.maxRange];

    public Player(String name) {
        this.name = name;
    }

    public int chooseNumber(int counter) {
        boolean validValue = false;
        int guess = -1;
        while (!validValue) {

            guess = Generator.generateNumber(Game.maxRange);
            validValue = true;
            for (int i = 0; i < counter; i++) {
                if ((guessed[i] == guess) || (Game.guessed[i] == guess)) {
                    validValue = false;
                    break;
                }
            }
            guessed[counter] = guess;
        }
        return guess;
    }

    public String getName() {
        return name;
    }

    public void startingValues() {
        for (int i = 0; i < Game.maxRange; i++) {
            guessed[i] = -1;
        }
    }

}