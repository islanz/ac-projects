package org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy;

public class SoldierEnemy extends Enemy {

    @Override
    public String toString() {
        return "Soldier";
    }

}
