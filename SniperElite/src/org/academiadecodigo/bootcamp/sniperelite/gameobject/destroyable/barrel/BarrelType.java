package org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.barrel;

public enum BarrelType {
    PLASTIC(20),
    WOOD(55),
    METAL(80);

    private final int maxDamage;

    BarrelType(int maxDamage) {
        this.maxDamage = maxDamage;
    }


    public int getMaxDamage() {
        return maxDamage;
    }

}
