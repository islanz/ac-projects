package org.academiadecodigo.bootcamp.sniperelite.gameobject.friendly;

import org.academiadecodigo.bootcamp.sniperelite.gameobject.GameObject;

public class Tree extends GameObject {

    @Override
    public String getMessage() {
        return "I am a tree.";
    }
}
