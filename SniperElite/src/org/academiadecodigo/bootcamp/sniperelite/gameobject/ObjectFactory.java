package org.academiadecodigo.bootcamp.sniperelite.gameobject;

import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.barrel.Barrel;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.barrel.BarrelType;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy.ArmouredEnemy;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy.SoldierEnemy;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.friendly.Tree;

public class ObjectFactory {

    public static GameObject createObject() {
        GameObject gameObject;
        double randomEntity = Math.random(); // randomEntity [0-2] -> Tree; randomEntity [3-6] -> Soldier; randomEntity [7-9] -> ArmouredSoldier

        if (randomEntity < 0.2f) {
            gameObject = new Tree();

        } else if (randomEntity < 0.5f) {
            gameObject = new SoldierEnemy();

        } else if (randomEntity < 0.8f) {
            gameObject = new ArmouredEnemy();
        } else {
            gameObject = new Barrel(BarrelType.values()[(int) (Math.random() * 3)]);
        }

        return gameObject;
    }

}
