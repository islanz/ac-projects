package org.academiadecodigo.bootcamp.sniperelite;

import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.Destroyable;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy.Enemy;

public class SniperRifle {

    private final int bulletDamage = 15;
    private int bulletsFired;

    public SniperRifle() {

    }


    public void shoot(Destroyable destroyable) {
        int randomShootDamage = (int) (Math.random() * (bulletDamage + 1));
        if (randomShootDamage == 0) {
            System.out.println("Didn't shoot!");
            return;
        }

        System.out.println("Hit " + randomShootDamage + " to " + destroyable);
        bulletsFired++;
        destroyable.hit(randomShootDamage);
    }

    public int getBulletsFired() {
        return bulletsFired;
    }
}
