package org.academiadecodigo.bootcamp.sniperelite;

import org.academiadecodigo.bootcamp.sniperelite.gameobject.GameObject;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.ObjectFactory;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.Destroyable;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy.Enemy;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.friendly.Tree;

public class Game {

    private GameObject[] gameObjects = new GameObject[OBJECTS_CREATED];
    private SniperRifle sniperRifle = new SniperRifle();

    public static final int OBJECTS_CREATED = 20;

    public Game() {

    }


    public void start() {

        gameObjects = createObjects(OBJECTS_CREATED);

        for (GameObject gameObject : gameObjects) {

            if (!(gameObject instanceof Destroyable)) {
                System.out.println((gameObject.getMessage()));
                continue;
            }

            Destroyable destroyable = (Destroyable) gameObject;
            System.out.println(destroyable);

            while (!destroyable.isDestroyed()) {
                sniperRifle.shoot(destroyable);
            }
        }

        System.out.println(sniperRifle.getBulletsFired()+ " shots fired.");

    }



    public GameObject[] createObjects(int entityNumber) {

        GameObject[] gameObjects = new GameObject[entityNumber];

        for (int i = 0; i < entityNumber; i++) {
            gameObjects[i] = ObjectFactory.createObject();
        }

        return gameObjects;
    }

}
