package org.academiadecodigo.bootcamp.battle;

public class Arena {

    private final Player playerOne;
    private final Player playerTwo;


    public Arena(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public void battle() {

        int round = 0;
        while (true) {

            round++;
            System.out.println("ROUND: " + round);

            if (playerOne.hasLost()) {
                //GAME OVER
                System.out.println("GAME OVER");
                System.out.println(playerOne.getName() + " has lost! " + playerTwo.getName() + " won! Congrats");
                return;
            }

            playerOne.attack(playerTwo);

            if (playerTwo.hasLost()) {
                //GAME OVER
                System.out.println("GAME OVER");
                System.out.println(playerTwo.getName() + " has lost! " + playerOne.getName() + " won! Congrats");
                return;
            }

            playerTwo.attack(playerOne);

            System.out.println("###############################################################");

        }

    }

}
