package org.academiadecodigo.bootcamp.battle.fighter;

public abstract class Fighter {

    private final String name;
    private final int attackDamage;
    private final int spellDamage;
    private int health;


    public Fighter(String name, int attackDamage, int spellDamage, int health) {
        this.name = name;
        this.attackDamage = attackDamage;
        this.spellDamage = spellDamage;
        this.health = health;
    }


    public void hit(Fighter fighter) {
        System.out.println("Inflicted " + attackDamage + " damage.");
        fighter.suffer((attackDamage));
    }


    public void cast(Fighter fighter) {
        System.out.println("Inflicted " + spellDamage + " damage.");
        fighter.suffer((spellDamage));
    }


    public void suffer(int dmg) {
        setHealth(dmg);
        System.out.println(getName() + " health: " + this.health);
    }


    public boolean isDead() {
        return health == 0;
    }


    public String getName() {
        return name;
    }


    public void setHealth(int dmg) {
        health = (health >= dmg) ? health - dmg : 0;
    }


    public int getHealth() {
        return health;
    }
}