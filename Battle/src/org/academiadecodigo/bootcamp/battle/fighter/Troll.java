package org.academiadecodigo.bootcamp.battle.fighter;

public class Troll extends Fighter {

    private static final int attackDamage = 15;
    private static final int spellDamage = 2;
    private static final int health = 200;


    public Troll(String name) {
        super(name, attackDamage, spellDamage, health);
    }


    @Override
    public void hit(Fighter fighter) {

        int random = (int) (Math.random() * 2);

        if (random == 1) {
            //attack
            System.out.println(super.getName() + " inflicted " + attackDamage + " damage.");
            fighter.suffer(attackDamage);
            return;
        }

        //don't attack
        System.out.println("I don't want to attack...");
        fighter.suffer(0);
    }

}
