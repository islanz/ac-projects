package org.academiadecodigo.bootcamp.battle.fighter;

public class Wizard extends Fighter {

    private static final int attackDamage = 5;
    private static final int spellDamage = 20;
    private static final int health = 50;

    private boolean shield;


    public Wizard(String name) {
        super(name, attackDamage, spellDamage, health);
    }

    @Override
    public void cast(Fighter fighter) {

        int random = (int) (Math.random() * 2);

        if (random == 1) {
            //attack
            System.out.println(super.getName() + " inflicted " + spellDamage + " damage.");
            fighter.suffer(spellDamage);
            return;
        }

        //don't attack
        System.out.println("GET A SHIELD");
        shield = true;

    }


    @Override
    public void suffer(int dmg) {

        if (shield) {
            System.out.println("Shield used");
            shield = false;
            return;
        }

        super.suffer(dmg);
        System.out.println(super.getName() + " health: " + super.getHealth());

    }
}
