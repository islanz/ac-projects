package org.academiadecodigo.tailormoons.file_analyzer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        String pathFileOne = "FileOne";

        try {
            // FILE READER
            FileReader fileReader = new FileReader(pathFileOne);
            BufferedReader reader = new BufferedReader(fileReader);


            // COUNT WORDS
            long numberWords = reader.lines()
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .count();

            //System.out.println("Words: " + numberWords);

            reader = new BufferedReader(new FileReader(pathFileOne));
            // GET FIRST BIGGER WORD
            Optional<String> firstBiggestWord = reader.lines()
                    .flatMap(line -> Arrays.stream(line.split(" "))
                            .filter(word -> word.length() > 3))
                    .findFirst();

            //System.out.println(firstBiggestWord);

            Optional<String> firstLongWord = Files.lines(Path.of(pathFileOne))
                    .flatMap(line -> Arrays.stream(line.split(" "))
                            .filter(word -> word.length() > 3))
                    .findFirst();
            //System.out.println("The first word longest than " + 3 + " characters: " + firstLongWord);


            // GET N BIGGEST WORDS
            reader.lines()
                    .flatMap(line -> Arrays.stream(line.split(" "))
                            .filter(word -> word.length() > 9))

                    .limit(5)
                    .forEach(System.out::println);


            // GET COMMON WORDS
            /*
            reader.lines()
                    .flatMap(line->Arrays.stream(line.split(" ")))
                    .max(Comparator.comparing(Entry::getValue))
                    .ifPresent(System.out::println);

             */


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
