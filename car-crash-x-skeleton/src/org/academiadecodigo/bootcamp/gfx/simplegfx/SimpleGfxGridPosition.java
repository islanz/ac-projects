package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.GridColor;
import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;

    /**
     * Simple graphics position constructor
     *
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(SimpleGfxGrid grid) {
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);

        int posX = grid.columnToX(getCol());
        int posY = grid.rowToY(getRow());
        rectangle = new Rectangle(posX, posY, grid.getCellSize(), grid.getCellSize());
        show();

        this.simpleGfxGrid = grid;
    }

    /**
     * Simple graphics position constructor
     *
     * @param col  position column
     * @param row  position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid) {
        super(col, row, grid);int posX = grid.columnToX(getCol());

        int posY = grid.rowToY(getRow());
        rectangle = new Rectangle(posX, posY, grid.getCellSize(), grid.getCellSize());
        show();

        this.simpleGfxGrid = grid;
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        rectangle.fill();

    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        rectangle.delete();
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {

        int posStartX = getCol();
        int posStartY = getRow();

        super.moveInDirection(direction, distance);
        int moveX = simpleGfxGrid.columnToX(getCol()) - simpleGfxGrid.columnToX(posStartX);
        int moveY = simpleGfxGrid.rowToY(getRow()) - simpleGfxGrid.rowToY(posStartY);
        rectangle.translate(moveX, moveY);

    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        super.setColor(color);
        rectangle.setColor(SimpleGfxColorMapper.getColor(color));
    }
}
