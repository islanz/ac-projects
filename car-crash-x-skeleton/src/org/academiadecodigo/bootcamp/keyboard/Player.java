package org.academiadecodigo.bootcamp.keyboard;

import org.academiadecodigo.bootcamp.car.Car;
import org.academiadecodigo.bootcamp.car.CarType;
import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player extends Car implements KeyboardHandler {


    /**
     * Picture of movable car
     */
    Picture picture;

    /**
     * Constructs a new car
     *
     * @param pos the initial car position
     */
    public Player(GridPosition pos) {
        super(pos, CarType.PLAYER);

        picture = new Picture(pos.getCol() * 15 + 10, pos.getRow() * 15 + 10, "assets/car-up.png");
        picture.draw();
        startListening(picture);
    }

    public  void startListening(Picture picture) {

       // KeyboardListener keyboardListener = new KeyboardListener(picture);
        Keyboard keyboard = new Keyboard(this);

        KeyboardEvent left = new KeyboardEvent();
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        left.setKey(KeyboardEvent.KEY_LEFT);

        keyboard.addEventListener(left);

        KeyboardEvent right = new KeyboardEvent();
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        right.setKey(KeyboardEvent.KEY_RIGHT);

        keyboard.addEventListener(right);

        KeyboardEvent up = new KeyboardEvent();
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        up.setKey(KeyboardEvent.KEY_UP);

        keyboard.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        down.setKey(KeyboardEvent.KEY_DOWN);

        keyboard.addEventListener(down);
    }

    @Override
    public void move() {
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (super.isCrashed()) {
            return;
        }

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                picture.load("assets/car-left.png");
                accelerate(GridDirection.LEFT, 1);
                picture.translate(-15, 0);
                break;
            case KeyboardEvent.KEY_RIGHT:
                picture.load("assets/car-right.png");
                accelerate(GridDirection.RIGHT, 1);
                picture.translate(15, 0);
                break;
            case KeyboardEvent.KEY_UP:
                picture.load("assets/car-up.png");
                accelerate(GridDirection.UP, 1);
                picture.translate(0, -15);
                break;
            case KeyboardEvent.KEY_DOWN:
                picture.load("assets/car-down.png");
                accelerate(GridDirection.DOWN, 1);
                picture.translate(0, 15);
                break;
        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }


}
