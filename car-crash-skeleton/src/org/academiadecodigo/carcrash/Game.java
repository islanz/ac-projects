package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private final int delay;

    private int timesMoved = 0;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // Update screen
            Field.draw(cars);

        }

    }


    private void moveAllCars() {

        timesMoved++;

        for (Car car : cars) {

            if (car.isCrashed()) {
                continue;
            }

            if (timesMoved % (50000 / car.getCarType().getSpeed()) != 0) {
                continue;
            }

            int posX = car.getPos().getCol();
            int posY = car.getPos().getRow();

            Direction direction = car.move();
            switch (direction) {
                case UP:
                    posY--;
                    break;

                case DOWN:
                    posY++;
                    break;

                case LEFT:
                    posX--;
                    break;

                case RIGHT:
                    posX++;
                    break;
            }

            car.setPos(posX, posY);
            car.collisionDetector(cars);
        }
    }

}
