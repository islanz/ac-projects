package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Helicopter extends Car{

    private static final CarType CAR_TYPE = CarType.HELICOPTER;
    private static final String NAME = "H";

    public Helicopter(Position position) {
        super(position, NAME, CAR_TYPE);
    }
}
