package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Mustang extends Car {

    private static final CarType CAR_TYPE = CarType.MUSTANG;
    private static final String NAME = "M";

    public Mustang(Position position) {
        super(position, NAME, CAR_TYPE);
    }
}
