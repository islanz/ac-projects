package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Fiat extends Car {

    private static final CarType CAR_TYPE = CarType.FIAT;
    private static final String NAME = "F";

    public Fiat(Position position) {
        super(position, NAME, CAR_TYPE);
    }


}
