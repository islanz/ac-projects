package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Tank extends Car{

    private static final CarType CAR_TYPE = CarType.TANK;
    private static final String NAME = "T";

    public Tank(Position position) {
        super(position, NAME, CAR_TYPE);
    }


    @Override
    public void setCrashed(boolean crashed) {
    }
}
