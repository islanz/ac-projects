package org.academiadecodigo.tailormoons.udp_uppercase.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.Charset;

public class Server {

    public static void main(String[] args) {

        int portNumber = 6000;
        int destinationPort;
        byte[] buffer = new byte[2048];
        String message = "";

        DatagramSocket socket;
        DatagramPacket packet = null;

        try {
            socket = new DatagramSocket(portNumber);

        } catch (SocketException ex) {
            System.out.println(ex.toString());
            return;
        }

        while (!message.equals("CLOSE")) {

            try {
                packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }

            message = new String(buffer);
            message = message.toUpperCase();
            message  = message.split("\0")[0];
            System.out.println(message);

            buffer = message.getBytes();

            destinationPort = packet.getPort();

            try {
                packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("localhost"), destinationPort);
                socket.send(packet);
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }

            buffer = new byte[2048];

        }

        socket.close();

    }

}
