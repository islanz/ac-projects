package org.academiadecodigo.tailormoons.udp_uppercase.client;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int portNumber = 7000;
        int destinationPort = 6000;
        byte[] buffer;
        String message = "";

        DatagramSocket socket;
        DatagramPacket packet;

        try {
            socket = new DatagramSocket(portNumber);

        } catch (SocketException ex) {
            System.out.println(ex.toString());
            return;
        }

        while (!message.equals("CLOSE")) {

            message = scanner.nextLine();
            buffer = message.getBytes();

            try {
                packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("localhost"), destinationPort);
                socket.send(packet);
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }

            try{
                packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
                message = new String(buffer);
                System.out.println(message);
            }catch (IOException ex) {
                System.out.println(ex.toString());
            }

        }

        socket.close();

    }

}
