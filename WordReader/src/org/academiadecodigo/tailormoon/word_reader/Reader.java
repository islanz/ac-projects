package org.academiadecodigo.tailormoon.word_reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Reader implements Iterable<String> {

    private String path;

    public Reader(String path) {
        this.path = path;
    }


    @Override
    public Iterator<String> iterator() {
        return new ReaderIterator();
    }


    private class ReaderIterator implements Iterator<String> {

        private int index = -1;
        private String line = "";
        private String[] words;
        private BufferedReader bufferedReader = null;

        public ReaderIterator() {
            try {
                FileReader fileReader = new FileReader(path);
                bufferedReader = new BufferedReader(fileReader);

                words = bufferedReader.readLine().split(" ");

            } catch (FileNotFoundException ex) {
                System.out.println("File not found. Might be a wrong path! Check the path and try again.");

            } catch (IOException ex) {
                System.out.println("No lines found in the file");

            }

        }


        @Override
        public boolean hasNext() {

            if (words == null) {
                System.out.println("File not found");
                return false;
            }

            if (index + 1 >= words.length) {
                try {
                    if ((line = this.bufferedReader.readLine()) == null) {
                        close();
                        return false;
                    }
                } catch (IOException ex) {
                    System.out.println("File not found!");
                }

                words = line.split(" ");
                index = -1;
            }
            return true;
        }


        @Override
        public String next() {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            index++;
            return words[index];
        }


        private void close() {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }

    }
}
