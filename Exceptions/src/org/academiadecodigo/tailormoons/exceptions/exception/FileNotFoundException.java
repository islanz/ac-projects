package org.academiadecodigo.tailormoons.exceptions.exception;

public class FileNotFoundException extends FileException{

    public FileNotFoundException() {
        super("File not found!");
    }
}
