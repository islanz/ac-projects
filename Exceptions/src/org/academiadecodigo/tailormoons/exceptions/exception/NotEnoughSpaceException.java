package org.academiadecodigo.tailormoons.exceptions.exception;

public class NotEnoughSpaceException extends FileException {

    public NotEnoughSpaceException() {
        super("Not enough space");
    }
}
