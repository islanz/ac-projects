package org.academiadecodigo.tailormoons.exceptions.exception;

public class NotEnoughPermissionsException extends FileException {

    public NotEnoughPermissionsException() {
        super("Not enough permissions");
    }
}
