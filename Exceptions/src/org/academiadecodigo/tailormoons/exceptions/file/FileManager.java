package org.academiadecodigo.tailormoons.exceptions.file;

import org.academiadecodigo.tailormoons.exceptions.exception.FileException;
import org.academiadecodigo.tailormoons.exceptions.exception.FileNotFoundException;
import org.academiadecodigo.tailormoons.exceptions.exception.NotEnoughPermissionsException;
import org.academiadecodigo.tailormoons.exceptions.exception.NotEnoughSpaceException;

public class FileManager {

    private File[] files = new File[5];
    private int filesCreated;
    private boolean logged;


    public void login() {
        logged = true;
    }


    public void logout() {
        logged = false;
    }


    public File getFile(String name) throws FileNotFoundException {
        for (File file : files) {

            if (file == null) {
                continue;
            }

            if (file.getName().equals(name)) {
                return file;
            }
        }

        throw new FileNotFoundException();
    }


    public void createFile(String name) throws FileException {

        if (filesCreated >= 5) {
            throw new NotEnoughSpaceException();
        }

        if (!logged) {
            throw new NotEnoughPermissionsException();
        }

        files[filesCreated] = new File(name);
        filesCreated++;
    }
}
