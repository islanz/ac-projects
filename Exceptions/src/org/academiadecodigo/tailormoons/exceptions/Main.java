package org.academiadecodigo.tailormoons.exceptions;

import org.academiadecodigo.tailormoons.exceptions.exception.FileException;
import org.academiadecodigo.tailormoons.exceptions.exception.FileNotFoundException;
import org.academiadecodigo.tailormoons.exceptions.file.File;
import org.academiadecodigo.tailormoons.exceptions.file.FileManager;

public class Main {

    public static void main(String[] args) {

        int counter = 0;

        FileManager fileManager = new FileManager();

        while (counter < 7) {

            counter++;

            if (counter < 3) {
                fileManager.login();
            } else {
                fileManager.logout();
            }

            try {
                fileManager.createFile("File " + counter);
                System.out.println("Created File " + counter);
            } catch (FileException ex) {
                System.out.println(ex.getMessage());
            }
        }

        counter = 0;
        while (counter < 7) {

            counter++;

            try {
                File file = fileManager.getFile("File " + counter);
                System.out.println(file.getName());
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

}
