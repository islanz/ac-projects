package org.academiadecodigo.bootcamp.AccountManager;

public enum Products {
    CAR(25000, 4),
    BICYCLE(250, 25),
    SHOES(40, 345),
    BEER(1, 9999),
    GIRLFRIEND(218736765, 0);

    private double price;
    private int amount;

    Products(double price, int amount) {
        this.price = price;
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount -= amount;
    }

}
