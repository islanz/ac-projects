package org.academiadecodigo.bootcamp.AccountManager;

public class Person {

    private final Wallet wallet;
    private final Account account;
    private int pin;

    public Person(Wallet wallet, Account account, int pin) {
        this.wallet = wallet;
        this.account = account;
        this.pin = pin;
    }

    public void withdrawAccount(double money) {
        double total = wallet.deposit(account.withdraw(money, pin));
        System.out.println("Wallet balance:  " + total);
        System.out.println("Account balance: " + account.getBalance());
    }

    public void depositWallet(double money) {
        wallet.deposit(money);
        System.out.println("Wallet balance:  " + wallet.getBalance());
        System.out.println("Account balance: " + account.getBalance());
    }

    public void saveToAccount(double money) {
        double total = account.deposit(wallet.withdraw(money));
        System.out.println("Wallet balance:  " + wallet.getBalance());
        System.out.println("Account balance: " + total);
    }

    public boolean payFromWallet(double money) {
        double success = wallet.withdraw(money);
        System.out.println("Wallet balance:  " + wallet.getBalance());
        System.out.println("Account balance: " + account.getBalance());

        return success == 0 ? false : true;
    }

}
