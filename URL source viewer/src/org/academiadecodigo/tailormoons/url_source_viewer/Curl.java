package org.academiadecodigo.tailormoons.url_source_viewer;

import java.io.IOException;

public class Curl {

    private static final String USAGE = "USAGE: java Curl <hostname>";

    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println(USAGE);
            return;
        }

        String hostName = args[0];
        Viewer viewer;

        try {
            viewer = new Viewer(hostName);
            viewer.start();
        } catch (IOException ex) {
            System.out.println("Can't connect to the host");
            ex.printStackTrace();
        }

    }

}
