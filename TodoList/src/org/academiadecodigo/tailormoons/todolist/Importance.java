package org.academiadecodigo.tailormoons.todolist;

public enum Importance {
    HIGH(1),
    MEDIUM(2),
    LOW(3);

    private int value;

    Importance(int value) {
        this.value = value;
    }


    public int getValue() {
        return value;
    }
}
